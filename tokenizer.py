import sys
import subprocess

with open('tokens.md', 'r') as tokenfile:
    tokens = tokenfile.readlines()

fore_call = ['echo', '-n']
aft_call = ['gpg', '--print-md', 'sha512']
sha_tokens = []
for token in tokens:
    call1=fore_call
    call1.append(token.strip())
    call2 = aft_call
    try:
        p1 = subprocess.Popen(call1, stdout=subprocess.PIPE)
        p2 = subprocess.Popen(call2, stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        sha_token = p2.communicate()[0]
        sha_tokens.append((''.join(sha_token.split())).lower())
    except:
        print 'UH OH!'
        sys.exit(1)

with open('shatokens.md', 'w') as tokenfile:
    tokenfile.write('\n'.join(sha_tokens))
