#!/bin/bash

if [[ -z $1 ]]; then
    echo "missing name of chroot, exiting"
    exit
fi

if [[ -z $2 ]]; then
    echo "missing target for chroot, exiting"
    exit
fi

if [[ -d $2 ]]; then
    echo "chroot exists already, exiting"
    exit
fi

chroot=$2
arch="i386"
branch="stretch"
upstream="https://deb.debian.org/debian"

directory=$(pwd)/$chroot
description="chroot for $1 at ${directory}"
users=$USER
groups="sbuild"

if [[ ! -d $chroot ]]; then
    mkdir -p "$chroot"
else
    echo "chroot dir exists, exiting"
    exit
fi

debootstrap --arch $arch $branch "$chroot"/ $upstream
cat >> /etc/schroot/schroot.conf << EOF
[$1]
description=$description
type=directory
directory=$directory
users=$users
groups=$groups
root-groups=root
aliases=$1
personality=linux32
profile=default
EOF
