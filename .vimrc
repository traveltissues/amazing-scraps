" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=72

  augroup END

else

"  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
if has('syntax') && has('eval')
  packadd matchit
endif

filetype indent plugin on
syn on se title

set softtabstop=2
colorscheme torte

" highlight syntax
syntax on
" show line numbers
set number
" show a ruler
set ruler
" display a ruler for max width
set cc=80
" indent new lines
set smartindent
" auto indent is 2 spaces
set shiftwidth=2
" use spaces in place of a <TAB>
set expandtab
" tabs are 2 spaces in a file
set tabstop=2
" using <TAB> at line start insert shiftwidth spaces
set smarttab
" display '-' for trailing space, '>-' for tab, '_' for non breakable space
set listchars=tab:>-,trail:-,nbsp:_
set list

"This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>