#!/bin/bash

if [[ ! -d "preview" ]]; then
    mkdir "preview"
fi

file_name=$(basename $1) 
output=preview/$file_name.html 
pandoc -s -o $output $1
firefox $output
