#!/bin/bash
set -ex
apt-get update
apt-get install -y \
    python3 \
    fuse \
    bubblewrap \
    python3-pip \
    python3-dev
apt-get install -y \
    bzr \
    git \
    lzip \
    patch \
    python3-arpy \
    python3-gi \
    cython3
apt-get install -y -t stretch-backports \
    gir1.2-ostree-1.0 \
    ostree
